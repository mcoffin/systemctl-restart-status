#pragma once
#include <string>
#include <ostream>

namespace color {
	class ColorCode {
	public:
		ColorCode(unsigned int colorIndex, int effectCode, bool background);
		~ColorCode();
		std::string getCode() const;
		unsigned int colorCode() const;
	private:
		unsigned int colorIndex;
		int effectCode;
		bool background;
	};
	extern std::string reset;
	extern ColorCode cyan;
	extern ColorCode boldRed;
};

extern std::ostream& operator<<(std::ostream& os, const color::ColorCode& c);
