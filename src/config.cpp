#include "config.hpp"
#include "color.hpp"
#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <cstdio>
#include <cstring>

Config::Config(int argc, char *argv[]):
	useSudo(false),
	useJournal(false),
	tail(false)
{
	opterr = 0;
	int opt;
	while ((opt = getopt(argc, argv, "sf")) != -1) {
		switch (opt) {
			case 's':
				useSudo = true;
				break;
			case 'f':
				tail = true;
				useJournal = true;
				break;
			case 'j':
				useJournal = true;
				break;
			case '?':
				if (isprint(optopt)) {
					std::cerr << color::boldRed << "Unknown option" << color::reset << ": -" << static_cast<char>(optopt) << std::endl;
				} else {
					fprintf(stderr, "Unknown option character: \\x%x\n", optopt);
				}
				exit(1);
			default:
				exit(1);
		}
	}
	auto trailingArgCount = argc - optind;
	units = std::vector<char *>(trailingArgCount);
	memcpy(units.data(), &argv[optind], trailingArgCount * sizeof(char *));
}

Config::~Config()
{
}

std::ostringstream Config::getBaseCommand()
{
	std::ostringstream ret("");
	if (useSudo) {
		ret << "sudo ";
	}
	return ret;
}

std::ostringstream Config::getStatusCommand()
{
	auto ret = this->getBaseCommand();
	if (this->useJournal) {
		ret << "journalctl ";
		if (this->tail) {
			ret << "-f ";
		}
		ret << "-u ";
	} else {
		ret << "systemctl status ";
	}
	return ret;
}
