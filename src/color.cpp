#include "color.hpp"
#include <string>
#include <ostream>
#include <sstream>

inline static unsigned int colorCodeBase(bool background)
{
	if (background) {
		return 40;
	} else {
		return 30;
	}
}

namespace color {
	ColorCode::ColorCode(unsigned int colorIndex, int effectCode, bool background):
		colorIndex(colorIndex),
		effectCode(effectCode),
		background(background)
	{
	}
	ColorCode::~ColorCode()
	{
	}
	unsigned int ColorCode::colorCode() const
	{
		return colorCodeBase(this->background) + this->colorIndex;
	}
	std::string ColorCode::getCode() const
	{
		std::ostringstream ret("");
		ret << "\033[";
		if (this->effectCode >= 0) {
			ret << this->effectCode << ';';
		}
		ret << this->colorCode() << "m";
		return ret.str();
	}
	std::string reset = "\033[0m";
	ColorCode cyan = ColorCode(6, -1, false);
	ColorCode boldRed = ColorCode(1, 1, false);
};

std::ostream& operator<<(std::ostream& os, const color::ColorCode& c) {
	os << c.getCode();
	return os;
}

