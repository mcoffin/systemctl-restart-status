#pragma once

#include <memory>
#include <vector>
#include <sstream>

class Config {
public:
	Config(int argc, char *argv[]);
	~Config();

	bool useSudo;
	bool useJournal;
	bool tail;

	inline std::vector<char *>& getUnits() {
		return units;
	}

	std::ostringstream getBaseCommand();
	std::ostringstream getStatusCommand();
private:
	std::vector<char *> units;
};
