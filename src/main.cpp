#include "color.hpp"
#include "config.hpp"
#include <iostream>
#include <vector>
#include <ostream>
#include <sstream>
#include <cstring>

int main(const int argc, char *argv[]) {
	Config config(argc, argv);

	int status = 0;

	for (char *unit : config.getUnits()) {
		auto restartCommand = config.getBaseCommand();
		auto statusCommand = config.getStatusCommand();
		std::cout << "restarting " << color::cyan << unit << color::reset << std::endl;
		restartCommand << "systemctl restart " << unit;
		status = system(restartCommand.str().c_str());
		if (status) {
			return status;
		}
		statusCommand << unit;
		system(statusCommand.str().c_str());
	}

	return 0;
}
