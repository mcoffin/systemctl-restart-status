# systemctl-restart-status

Convenience command for restarting a systemd service and then getting it's status

# Usage

```bash
systemctl-restart-status [-s] UNITS...
```

## Example

```bash
systemctl-restart-status systemd-{networkd,resolved}.service
# Is effectively equivalent to:
services=(systemd-{networkd,resolved}.service)
for unit in ${services[@]}; do
    systemctl restart "$unit"
    if [ $? -ne 0 ]; then
        exit $?;
    fi
    systemctl status "$unit"
done
```

# Building

`systemctl-restart-status` uses [meson](https://mesonbuild.com) for generating it's build files.

```bash
# Configure
meson --prefix /usr/local --buildtype release builds/release
# Build
ninja -C builds/release
# Install
sudo ninja -C builds/release install
```
